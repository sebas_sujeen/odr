#include "common_defines.h"


int main()
{
	time_t ticks;
	int flag;
	char msg[MAX_MSG_SZ];
	char ip[INET_ADDRSTRLEN];
	int port, sockfd;
	char hostname[HOST_NAME_MAX];
	get_lhostname(hostname, sizeof(hostname));

	sockfd = bind_domain_socket(SERVER_SUNPATH);
	while (1) {
		flag = msg_recv(sockfd, msg, ip, &port );
		printf("<Server> at %s responding to request from %s\n", hostname, get_name(ip));
		memset(msg, 0x0, sizeof(msg));
		ticks = time(NULL);
		snprintf(msg, sizeof(msg), "%.24s\r\n", ctime(&ticks));
		msg_send(sockfd, ip, port, msg,  flag);
	}
	return 0;
}
