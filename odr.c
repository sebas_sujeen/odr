#include "common_defines.h"

static void handle_local_server_request(struct odr_socket_info *info, int port)
{
	char ip[INET_ADDRSTRLEN];
	char msg[MAX_MSG_SZ] = "xx";
	int t_port;
	int flag;
	
	/* trying to communicate with the local server */
	msg_send(info->dom_socket, can_ip, SERVER_PORT, msg, 0);
	/* store the ephemereal port that we got from the client, so that
	   it could be later used for lookup */
	t_port = port;
	/* receive the timestamp from the server */
	flag = msg_recv(info->dom_socket, msg, ip, &port);
	/* forward it to the client */
	msg_send(info->dom_socket, ip, t_port, msg, flag);
	return;

}

static void handle_odr_connection(struct odr_socket_info *info)
{
	/* need to select on the domain socket and all the link sockets */
	int max_fd = -1, i;
	fd_set rset;
	char ip[INET_ADDRSTRLEN];
	char msg[MAX_MSG_SZ];
	struct t_pkt_info t_info; 
	int port, t_port, index = -1, flag;
	struct ethernet_frame *eframe;
	memset(&t_info, 0x0, sizeof(t_info));
	max_fd = max(max_fd, info->dom_socket);

	for (i = 0; i < info->count; i++) {
		max_fd = max(max_fd, info->sockfds[i]);
	}
	max_fd += 1;

	for (;;) {
		FD_ZERO(&rset);
		FD_SET(info->dom_socket, &rset);
		for (i = 0; i < info->count; i++) {
			FD_SET(info->sockfds[i], &rset);
		}
		_select(max_fd, &rset, NULL, NULL, NULL);
		if (FD_ISSET(info->dom_socket, &rset)) {
			/* handle domain socket stuff */

			/* the way to think about this is that the ODR process receives messages
			   from both the client and the server. It might look like we need to handle 
			   the two cases, but in reality it is just one case. We don't need to handle
			   the message from server explicitly, the reason is , the message from the server
			   is received because the client initiated the request */
			flag = msg_recv(info->dom_socket, msg, ip, &port );
			
			/* in the client there are two cases, the client might try to reach the server running on its 
			   local machine, or the server on a remote machine. */
			if (strcmp(can_ip, ip) == 0) {
				/* handle request to the server running on local vm */
#if DEBUG
				printf("<ODR> Handling request to local server\n");
#endif
				handle_local_server_request(info, port);
				continue;
			}
			/* fill the src ip with its own canonical ip */
			strcpy(t_info.src_ip, can_ip);
			/* fill the destn ip from the dest ip address we got via the domain socket */
			strcpy(t_info.dst_ip, ip);
			/* this port is not actually required, but useful for communicating the results 
			 * back to the client, in case of multiple clients from the same vm */
			t_info.src_port = port;
			/* once the pkt reaches the appropriate destn, this port is used to talk to the 
			 * server process */
			t_info.dst_port = SERVER_PORT;
			/* this function gets the current broadcast id and increments the original by 1 */
			t_info.flag = flag;
			t_info.aux_flag = 0;
			strcpy(t_info.msg, msg);
			printf("<ODR> Request from %s to %s\n", get_name(can_ip), get_name(ip));
			/* handle the hard case, trying to communicate with a non-local vm */
			
			/* first we check to see if a route exists for the ip and it is not stale */
			index = check_route_valid(ip);
			if (index != -1 && flag != ROUTE_DISCOVER) {
				/* got an entry that is valid, use it to send the data packet */
				t_info.type = PKT_DATA;
				t_info.hop_count = 0;
				printf("<ODR> Route for %s found in the routing table..Directly sending the DATA pkt\n", get_name(ip));
				eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
				write_frame(eframe, info, rtable[index].if_idx, PKT_DATA);
				free(eframe);
			} else {
				t_info.bcast_id = get_broadcast_id(can_ip);
				t_info.type = PKT_RREQ;
				t_info.hop_count = 0;
				int j;
				printf("<ODR> Route for %s not found in the routing table..Generating an RREQ with broadcast id : %d\n", get_name(ip), t_info.bcast_id);
				for (j = 0; j < info->count; j++) {
					/* no ifi_idx to ignore here */
					eframe = construct_frame(info, info->ifi_idx[j], &t_info, -1);
					/* send the RREQ to all the interfaces except the interface it came on */
					write_frame(eframe, info, info->ifi_idx[j], PKT_RREQ);
					free(eframe);
				}
			}
		}
		else {
			for (i = 0; i < info->count; i++) {
				if (FD_ISSET(info->sockfds[i], &rset)) {
					/* handle link layer stuff */
					struct ethernet_frame eframe;
					struct sockaddr_ll addr;
					socklen_t len = sizeof(addr);

					memset(&addr, 0x0, sizeof(addr));
					memset(&eframe, 0x0, sizeof(eframe));
					/* basically we get a ethernet frame from the socket, and we use Recvfrom()
					   so that we know which interface addr it came from. We need to enter the reverse
					   route once we got a packet ie) we get the src address where it came from from the 
					   odr data packet. so record this information in the routing table */
					Recvfrom(info->sockfds[i], &eframe, sizeof(eframe), 0,(void *) &addr, &len);
					/* this is the function where most of the work is done */
					handle_routing(info->sockfds[i], info, &eframe, &addr);
				}
			}
		}
	}
}

int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("Usage : %s <staleness in seconds>\n", argv[0]);
		exit(-1);
	}

	int sockfd, sockfd_dom, staleness;
	char msg[MAX_MSG_SZ];
	int port;
	char ip[INET_ADDRSTRLEN];
	/* odr sockets info */
	struct odr_socket_info *ls_info;
	struct vm_iface_info *head = get_vm_iface_info();

	ls_info = bind_link_sockets(head);
	sockfd_dom = bind_domain_socket(ODR_DOM_PATH);
	ls_info->dom_socket = sockfd_dom;
	staleness = atoi(argv[1]);
	printf("<ODR> Staleness is %d\n", staleness);
	/* initialize the routing table maintained by the ODR */
	init_odr_route_table(staleness);
	/* initialize the addr table which keeps the relationship between
	 * client's ephemeral port and its sunpath */
	init_odr_addr_table();
	/* the packet queue is when stale routes are found when trying to route
	 * RREP/DATA pkts, and in that scenario, we store these pkts in a queue and 
	 * generate a new RREQ  */
	init_pkt_queue();
	init_dup_list();
	handle_odr_connection(ls_info);
	return  0;
}
