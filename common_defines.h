
#ifndef _COMMON_DEFINES_H
#define _COMMON_DEFINES_H
#include "unp.h"
#include "unpifi.h"
#include <assert.h>
#include "hw_addrs.h"
#include <net/ethernet.h>
#include <netpacket/packet.h>
#include <net/if_arp.h>

#define ODR_DOM_PATH "/tmp/odr_dom_3254"
#define ROUTE_DISCOVER 0x2
#define RREP_ALREADY_SENT 0x3
#define MAX_MSG_SZ 512
#define DELIMITER "|"
#define ODR_PROTOID 0x3254
#define SERVER_PORT 31337
#define SERVER_SUNPATH "/tmp/odrserver_3254"
#define DEBUG 1
enum {
	VM1_IDX,
	VM2_IDX,
	VM3_IDX,
	VM4_IDX,
	VM5_IDX,
	VM6_IDX,
	VM7_IDX,
	VM8_IDX,
	VM9_IDX,
	VM10_IDX,
};
enum {
	PKT_RREQ,
	PKT_RREP,
	PKT_DATA,
};

#define VM1_ADDR  "130.245.156.21"
#define VM2_ADDR  "130.245.156.22"
#define VM3_ADDR  "130.245.156.23"
#define VM4_ADDR  "130.245.156.24"
#define VM5_ADDR  "130.245.156.25"
#define VM6_ADDR  "130.245.156.26"
#define VM7_ADDR  "130.245.156.27"
#define VM8_ADDR  "130.245.156.28"
#define VM9_ADDR  "130.245.156.29"
#define VM10_ADDR "130.245.156.20"

#define ARRAY_SIZE(name) sizeof(name)/sizeof(name[0])
void get_dest_entry(int, struct sockaddr_un *);
void add_dest_entry(int dest_port, struct sockaddr_un *addr);
/* contains information about the interface other than eth0 and lo */
struct vm_iface_info {
	int if_index;
	uint8_t if_haddr[IF_HADDR];
	char ip_addr[INET_ADDRSTRLEN];
	struct vm_iface_info *next;
};

struct vm_addr_struct {
	int idx ;
	char addr[32];
};

/* this structure is mainly to pass information to construct_frame */
struct t_pkt_info {
	int type;
	char src_ip[INET_ADDRSTRLEN];
	char dst_ip[INET_ADDRSTRLEN];
	uint16_t src_port;
	uint16_t dst_port;
	int hop_count;
	int bcast_id;
	int msg_sz;
	int flag;
	int aux_flag;
	char msg[MAX_MSG_SZ - ( (6 *sizeof(int)) + (2 * INET_ADDRSTRLEN) + (2 * sizeof(uint16_t)))];
};
/* doubly linked list since it is simpler.. */
struct pkt_queue {
	struct t_pkt_info info;
	struct pkt_queue *next;
	struct pkt_queue *prev;
};

struct dup_rrep {
	int bcast_id;
	char src_ip[INET_ADDRSTRLEN];
	char dst_ip[INET_ADDRSTRLEN];
	struct dup_rrep *next;
	struct dup_rrep *prev;
};

struct pkt_queue *pqueue;
struct dup_rrep *dup_head;

struct odr_addr {
	char sun_path[HOST_NAME_MAX];
	int port;
	/* is permanent flag, should be set for server entries */
	int is_perm;
	struct odr_addr *next;
	struct odr_addr *prev;
};

struct odr_socket_info {
	/* no of socket fds*/
	int count;
	/* domain socket */
	int dom_socket;
	/* array of hwaddr */
	uint8_t **if_addrs;
	/* array of socket fds */
	int *sockfds;
	/* array of interface indexes */
	int *ifi_idx;
};

struct odr_route_table {
	char dst_ip[INET_ADDRSTRLEN];
	uint8_t next_hop[IF_HADDR];
	int if_idx;
	int hop_count;
	time_t timestamp;
	int valid;
	/* broadcast id */
	int br_id;
	int staleness;
};

struct ethernet_hdr {
	uint8_t dst_mac[IF_HADDR];
	uint8_t src_mac[IF_HADDR];
	uint16_t type_id;
};

struct odr_data_pkt {
	int type;
	char src_ip[INET_ADDRSTRLEN];
	char dst_ip[INET_ADDRSTRLEN];
	uint16_t src_port;
	uint16_t dst_port;
	int hop_count;
	int bcast_id;
	int msg_sz;
	int flag;
	int aux_flag;
	char msg_data[MAX_MSG_SZ - ( (6 *sizeof(int)) + (2 * INET_ADDRSTRLEN) + (2 * sizeof(uint16_t)))];
};

struct ethernet_frame {
	struct ethernet_hdr eh;
	struct odr_data_pkt odr_data;
};

struct odr_addr *odr_addr_head;
char *can_ip;



struct odr_route_table rtable[] = {
	{.dst_ip = VM1_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM2_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM3_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM4_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM5_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM6_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM7_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM8_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM9_ADDR,  .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
	{.dst_ip = VM10_ADDR, .next_hop = {0}, .if_idx = -1, .hop_count = INT_MAX, .timestamp = LONG_MAX, .valid = 0, .br_id = 0, .staleness = 0},
};

/* storing index might seem redundant, indeed we can get away with an array if char *, this is just
   for maintable code, where vm's indexes are not necessarily starting from 1 */
struct vm_addr_struct vm[] = {
	{.idx = VM1_IDX,  .addr = VM1_ADDR},
	{.idx = VM2_IDX,  .addr = VM2_ADDR},
	{.idx = VM3_IDX,  .addr = VM3_ADDR},
	{.idx = VM4_IDX,  .addr = VM4_ADDR},
	{.idx = VM5_IDX,  .addr = VM5_ADDR},
	{.idx = VM6_IDX,  .addr = VM6_ADDR},
	{.idx = VM7_IDX,  .addr = VM7_ADDR},
	{.idx = VM8_IDX,  .addr = VM8_ADDR},
	{.idx = VM9_IDX,  .addr = VM9_ADDR},
	{.idx = VM10_IDX, .addr = VM10_ADDR},
};


void init_dup_list()
{
	dup_head = Calloc(1, sizeof(*dup_head));
	dup_head->next = dup_head->prev = dup_head;
}

int lookup_dup_list(struct t_pkt_info *t_info)
{
	struct dup_rrep *t = dup_head->next, *fd;
	
	while (t != dup_head) {
		if (t_info->bcast_id == t->bcast_id && strcmp(t_info->src_ip, t->src_ip) == 0 && strcmp(t_info->dst_ip, t->dst_ip) == 0) {
			printf("Entry already exists in the duplicate list\n");
			return 1;
		}
		t = t->next;
	}

	printf("Adding new entry to the duplicate list\n\n");
	/* If we reach here, then we don't have a duplicate, insert this into the list */
	t = dup_head;

	struct dup_rrep *node = Calloc(1, sizeof(*node));
	fd = dup_head->next;
	node->next = fd;
	node->prev = dup_head;
	dup_head->next = node;
	fd->prev = node;
	node->bcast_id = t_info->bcast_id;
	strcpy(node->src_ip, t_info->src_ip);
	strcpy(node->dst_ip, t_info->dst_ip);
	return 0;
}

void init_pkt_queue()
{
	pqueue = Calloc (1, sizeof(*pqueue));
	pqueue->next = pqueue->prev = pqueue;
}

static char *get_canonical_ip(char *name)
{
	/* skip the "vm" part */
	char *idx = &name[2];
	int index, i;

	index = atoi(idx);
	assert(index > 0 && index <= 10);
	/* array is zero based */
	index -= 1;
	for (i = 0; i < ARRAY_SIZE(vm); i++) {
		if (vm[i].idx == index)
			return vm[i].addr;
	}
	return NULL;
}

static void get_lhostname(char *buf, size_t len)
{
	int ret;

	/* get hostname doesn't guarantee null termination */
	buf[len -1] = 0x0;
	ret = gethostname(buf, len - 1);
	if (ret == -1) {
		printf("error in gethostname\n");
		exit(-1);
	}
	return ;
}

static uint16_t get_eph_port()
{
	/* we rely on randomness to make sure we get a unique eph port */
	return (uint16_t ) rand();
}

static int _select(int nfds, fd_set *rset, fd_set *wset, fd_set *eset, struct timeval *tv)
{
	int ret;
again:
	if ( (ret = select(nfds, rset, wset, eset, tv)) < 0) {
		if (errno == EINTR)
			goto again;
		else
			err_sys("Select error\n");
	}
	return ret;
}

static char *get_sunpath(int sockfd)
{
	struct sockaddr_un *s = Calloc(1, sizeof(*s));
	socklen_t len = sizeof(*s);
	Getsockname(sockfd, (void *)s, &len);
	return s->sun_path;
}

static int is_sunpath_odr(char *sunpath)
{
	if (strcmp(sunpath, ODR_DOM_PATH) == 0)
		return 1;
	else
		return 0;
}

char *get_name(char *str)
{
	struct in_addr addr;
	struct hostent *h;
	
	memset(&addr, 0x0, sizeof(addr));
	inet_pton(AF_INET, str, &addr);
	h = gethostbyaddr((void *) &addr, sizeof(addr), AF_INET);
	return h->h_name;
}

static void msg_send(int sockfd, char *dest_ip, int dest_port, char *msg, int flag)
{
	/* first we append the dest_ip, dest_port, msg, flag into a single string, separated by delimiters */
	char str[MAX_MSG_SZ];
	char port_buf[sizeof("65535") + 1];
	char flag_buf[4];
	char *sun_path;
	int is_odr;
	struct sockaddr_un addr;

	memset(&addr, 0x0, sizeof(addr));
	memset(str, 0x0, sizeof(str));
	snprintf(port_buf, sizeof(port_buf), "%d", dest_port);
	snprintf(flag_buf, sizeof(flag_buf), "%d", flag);
	/* no bounds checking, because we trust the input */
	strcpy(str, dest_ip);
	strcat(str, DELIMITER);
	strcat(str, port_buf);
	strcat(str, DELIMITER);
	strcat(str, msg);
	strcat(str, DELIMITER);
	strcat(str, flag_buf);
	strcat(str, DELIMITER);
	/* sendto to the domain socket */
	sun_path = get_sunpath(sockfd);

	/* need to find first if the client/server is talking to the ODR or the other way around */
	is_odr = is_sunpath_odr(sun_path);
	if (is_odr) {
		/* distinguish between the client and the server by the dest port, if the packet
		   is to be sent to the server, the dest_port is SERVER_PORT else, it needs to be
		   sent to the client. Handle all this uniformly via the table lookup */
		get_dest_entry(dest_port, &addr);				 
	} else {
		/* client/server sends to the ODR, fill in with the ODR_DOM_PATH */
		addr.sun_family = AF_LOCAL;
		strcpy(addr.sun_path, ODR_DOM_PATH);
	}
	Sendto(sockfd, str, sizeof(str), 0, (void *) &addr, sizeof(addr));
	return;
}

static int msg_recv(int sockfd, char *msg, char *ip, int *port)
{
	char str[MAX_MSG_SZ];
	char *token, *sun_path;
	int flag, is_odr;
	struct sockaddr_un addr;
	socklen_t len = sizeof(addr);
	/* the message itself is a null terminated string, so no need to worry about
	   explicit NULL termination */
	Recvfrom(sockfd, str, sizeof(str), 0, (void *) &addr, &len);
	token = strtok(str, DELIMITER);
	assert(token != NULL);
	strcpy(ip, token);
	token = strtok(NULL, DELIMITER);
	assert(token != NULL);
	*port = atoi(token);
	token = strtok(NULL, DELIMITER);
	assert(token != NULL);
	strcpy(msg, token);
	token = strtok(NULL, DELIMITER);
	assert(token != NULL);
	flag = atoi(token);
	sun_path = get_sunpath(sockfd);

	/* check if the sun_path belongs to the odr */
	is_odr = is_sunpath_odr(sun_path);
	if (is_odr) {
		/* the odr has to check if the dst port is SERVER_PORT, in which case it is getting the
		   message from the server, so no need to record the entry in the table, if we get some
		   other port, let's store the entry in the table */
		if (*port != SERVER_PORT)
			add_dest_entry(*port, &addr);
	}
	return flag;
}




	struct hwa_info *
get_hw_addrs()
{
	struct hwa_info	*hwa, *hwahead, **hwapnext;
	int		sockfd, len, lastlen, alias, nInterfaces, i;
	char		*ptr, *buf, lastname[IF_NAME], *cptr;
	struct ifconf	ifc;
	struct ifreq	*ifr, *item, ifrcopy;
	struct sockaddr	*sinptr;

	sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

	lastlen = 0;
	len = 100 * sizeof(struct ifreq);	/* initial buffer size guess */
	for ( ; ; ) {
		buf = (char*) Malloc(len);
		ifc.ifc_len = len;
		ifc.ifc_buf = buf;
		if (ioctl(sockfd, SIOCGIFCONF, &ifc) < 0) {
			if (errno != EINVAL || lastlen != 0)
				err_sys("ioctl error");
		} else {
			if (ifc.ifc_len == lastlen)
				break;		/* success, len has not changed */
			lastlen = ifc.ifc_len;
		}
		len += 10 * sizeof(struct ifreq);	/* increment */
		free(buf);
	}

	hwahead = NULL;
	hwapnext = &hwahead;
	lastname[0] = 0;

	ifr = ifc.ifc_req;
	nInterfaces = ifc.ifc_len / sizeof(struct ifreq);
	for(i = 0; i < nInterfaces; i++)  {
		item = &ifr[i];
		alias = 0; 
		hwa = (struct hwa_info *) Calloc(1, sizeof(struct hwa_info));
		memcpy(hwa->if_name, item->ifr_name, IF_NAME);		/* interface name */
		hwa->if_name[IF_NAME-1] = '\0';
		/* start to check if alias address */
		if ( (cptr = (char *) strchr(item->ifr_name, ':')) != NULL)
			*cptr = 0;		/* replace colon will null */
		if (strncmp(lastname, item->ifr_name, IF_NAME) == 0) {
			alias = IP_ALIAS;
		}
		memcpy(lastname, item->ifr_name, IF_NAME);
		ifrcopy = *item;
		*hwapnext = hwa;		/* prev points to this new one */
		hwapnext = &hwa->hwa_next;	/* pointer to next one goes here */

		hwa->ip_alias = alias;		/* alias IP address flag: 0 if no; 1 if yes */
		sinptr = &item->ifr_addr;
		hwa->ip_addr = (struct sockaddr *) Calloc(1, sizeof(struct sockaddr));
		memcpy(hwa->ip_addr, sinptr, sizeof(struct sockaddr));	/* IP address */
		if (ioctl(sockfd, SIOCGIFHWADDR, &ifrcopy) < 0)
			perror("SIOCGIFHWADDR");	/* get hw address */
		memcpy(hwa->if_haddr, ifrcopy.ifr_hwaddr.sa_data, IF_HADDR);
		if (ioctl(sockfd, SIOCGIFINDEX, &ifrcopy) < 0)
			perror("SIOCGIFINDEX");	/* get interface index */
		memcpy(&hwa->if_index, &ifrcopy.ifr_ifindex, sizeof(int));
	}
	free(buf);
	return(hwahead);	/* pointer to first structure in linked list */
}

	void
free_hwa_info(struct hwa_info *hwahead)
{
	struct hwa_info	*hwa, *hwanext;

	for (hwa = hwahead; hwa != NULL; hwa = hwanext) {
		free(hwa->ip_addr);
		hwanext = hwa->hwa_next;	/* can't fetch hwa_next after free() */
		free(hwa);			/* the hwa_info{} itself */
	}
}
/* end free_hwa_info */

	struct hwa_info *
Get_hw_addrs()
{
	struct hwa_info	*hwa;

	if ( (hwa = get_hw_addrs()) == NULL)
		err_quit("get_hw_addrs error");
	return(hwa);
}

struct vm_iface_info *get_vm_iface_info(void)
{
	struct hwa_info *hwa;
	struct vm_iface_info *vm_head = NULL, **vm_pnext = &vm_head, *vm_node;

	for (hwa = Get_hw_addrs(); hwa != NULL; hwa = hwa->hwa_next) {
		if (strcmp(hwa->if_name, "eth0") == 0 || strcmp(hwa->if_name, "lo") == 0)
			continue;
		vm_node = Calloc(1, sizeof(*vm_node));
		vm_node->if_index = hwa->if_index;
		strcpy(vm_node->ip_addr, Sock_ntop_host(hwa->ip_addr, sizeof(*(hwa->ip_addr))));
		memcpy(vm_node->if_haddr, hwa->if_haddr, sizeof(hwa->if_haddr));
		*vm_pnext = vm_node;
		vm_pnext = &vm_node->next;
	}
	return vm_head;
}

void print_vm_iface_info(struct vm_iface_info *vm)
{
	struct vm_iface_info *t = vm;
	char *p;
	int i;
	printf("#########\n");
	while (t) {
		printf("\tifi_idx = %d, ip_addr = %s and mac addr :  ", t->if_index, t->ip_addr);
		for (i = 0; i < IF_HADDR; i++) {
			printf("%02x:", t->if_haddr[i]);
		}
		printf("\n");
		t = t->next;
	}
}

void prhwaddrs (void)
{
	struct hwa_info	*hwa, *hwahead;
	struct sockaddr	*sa;
	char   *ptr;
	int    i, prflag;

	printf("\n");

	for (hwahead = hwa = Get_hw_addrs(); hwa != NULL; hwa = hwa->hwa_next) {

		printf("%s :%s", hwa->if_name, ((hwa->ip_alias) == IP_ALIAS) ? " (alias)\n" : "\n");

		if ( (sa = hwa->ip_addr) != NULL)
			printf("         IP addr = %s\n", Sock_ntop_host(sa, sizeof(*sa)));

		prflag = 0;
		i = 0;
		do {
			if (hwa->if_haddr[i] != '\0') {
				prflag = 1;
				break;
			}
		} while (++i < IF_HADDR);

		if (prflag) {
			printf("         HW addr = ");
			ptr = hwa->if_haddr;
			i = IF_HADDR;
			do {
				printf("%.2x%s", *ptr++ & 0xff, (i == 1) ? " " : ":");
			} while (--i > 0);
		}

		printf("\n         interface index = %d\n\n", hwa->if_index);
	}

	free_hwa_info(hwahead);
}

int bind_domain_socket(char *path)
{
	struct sockaddr_un addr;
	int sockfd = Socket(AF_LOCAL, SOCK_DGRAM, 0 );

	memset(&addr, 0x0, sizeof(addr));
	unlink(path);
	addr.sun_family = AF_LOCAL;
	strcpy(addr.sun_path, path);
	Bind(sockfd, (void *) &addr, sizeof(addr));
	return sockfd;
}

void connect_domain_socket(int sockfd, char *path)
{
	struct sockaddr_un addr;

	memset(&addr, 0x0, sizeof(addr));
	addr.sun_family = AF_LOCAL;
	strcpy(addr.sun_path, path);
	Connect(sockfd, (void *) &addr, sizeof(addr));
}

void init_odr_route_table(int staleness)
{
	int i;
	/* make sure we set the routing table entry of the canonical ip corresponding to this hostname to be valid
	   so, if valid == 1 and ifi_idx == -1, then communication is local, no need to route the packet out */

	char hostname[HOST_NAME_MAX];
	get_lhostname(hostname, sizeof(hostname));
	can_ip = get_canonical_ip(hostname);

	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (strcmp(can_ip, rtable[i].dst_ip) == 0) {
			rtable[i].valid = 1;
			rtable[i].hop_count = 0;
		}
		rtable[i].staleness = staleness;
		rtable[i].timestamp = time(NULL);
	}
}
struct odr_socket_info *bind_link_sockets(struct vm_iface_info *vm)
{
	struct vm_iface_info *t = vm;
	struct sockaddr_ll l_addr;
	int count = 0, i = 0, j;

	memset(&l_addr, 0x0, sizeof(l_addr));
	while (t) {
		count ++;
		t = t->next;
	}
	struct odr_socket_info *ls_info = Calloc(1, sizeof(*ls_info));
	ls_info->sockfds = Malloc(sizeof(int) * count);
	ls_info->ifi_idx = Calloc(1, sizeof(int) * count);
	ls_info->if_addrs = Calloc(1, count * sizeof(char *));

	for (j = 0; j < count; j++) {
		ls_info->if_addrs[j] = Calloc(1, IF_HADDR );
	}
	ls_info->count = count;

	t = vm;
	while (t) {
		ls_info->sockfds[i] = Socket(PF_PACKET, SOCK_RAW, htons(ODR_PROTOID));
		l_addr.sll_family = PF_PACKET;
		l_addr.sll_ifindex = t->if_index;
		ls_info->ifi_idx[i] = t->if_index;
		l_addr.sll_hatype = ARPHRD_ETHER;
		l_addr.sll_pkttype = PACKET_OTHERHOST;
		l_addr.sll_halen = ETH_ALEN;
		memcpy(l_addr.sll_addr, t->if_haddr, sizeof(t->if_haddr));
		memcpy(ls_info->if_addrs[i], t->if_haddr, sizeof(t->if_haddr));
		/* unused bytes set to 0, by memset() before */
		Bind(ls_info->sockfds[i], (void *) &l_addr, sizeof(l_addr));

		i ++;
		t = t->next;
	}
	return ls_info;
}

void init_odr_addr_table()
{
	/* initialization adds the server's well defined port and sunpath to the table */
	struct odr_addr *node;
	odr_addr_head = node = Calloc(1, sizeof(*node));
	node->next = node->prev = node;
	node->port = SERVER_PORT;
	strcpy(node->sun_path, SERVER_SUNPATH);
	node->is_perm = 1;
}

void get_dest_entry(int dest_port, struct sockaddr_un *addr)
{
	struct odr_addr *t;
	t = odr_addr_head;
	do {
		if (t->port == dest_port) {
			addr->sun_family = AF_LOCAL;
			strcpy(addr->sun_path, t->sun_path);
			break;
		}
		t = t->next;	
	} while (t != odr_addr_head);
	return;
}

void add_dest_entry(int dest_port, struct sockaddr_un *addr)
{
	struct odr_addr *t = odr_addr_head;
	do {
		if (t->port == dest_port) {
			/* the destination port already exists, return */
			return;
		}
		t = t->next;
	} while (t->next != odr_addr_head);

	t = odr_addr_head;
	struct odr_addr *fd, *node;

	fd = t->next;
	node = Calloc(1, sizeof(*node));
	node->port = dest_port;
	strcpy(node->sun_path, addr->sun_path);
	node->next = fd;
	node->prev = t;
	fd->prev = node;
	t->next = node;
}

int check_route_valid(char *ip)
{
	int i;
	time_t ticks;

	for (i = 0 ; i < ARRAY_SIZE(rtable); i ++) {
		if (strcmp(ip, rtable[i].dst_ip) == 0 ) {
			/* check if the entry is "fresh"  and also has a valid next hop*/
			ticks = time(NULL);
			if (rtable[i].valid) {
				if (ticks < rtable[i].timestamp + rtable[i].staleness ) {
					/* update the timestamp */
					rtable[i].timestamp = time(NULL);
					return i;
				}
				else
					/* stale entry, set the valid to 0 */
					rtable[i].valid = 0;
			}
			break;
		}
	}
	return -1;
}

int check_route_valid_without_staleness(char *ip)
{
	int i;
	time_t ticks;

	for (i = 0 ; i < ARRAY_SIZE(rtable); i ++) {
		if (strcmp(ip, rtable[i].dst_ip) == 0  && rtable[i].valid == 1) {
			return i;
		}
	}
	return -1;
}
static inline uint8_t *get_src_mac(struct odr_socket_info *info, int ifi_index)
{
	assert(info->count > 0);
	int i;
	uint8_t *mac_addr = NULL;

	for (i = 0; i < info->count; i ++) {
		if (info->ifi_idx[i] == ifi_index) {
			mac_addr = info->if_addrs[i];
			break;
		}
	}
	assert(mac_addr != NULL);
	return mac_addr;
}


static void populate_hdr(struct ethernet_hdr *hdr, struct odr_socket_info *info,  int ifi_index, int type, int index)
{
	uint8_t *src_mac = get_src_mac(info, ifi_index);
	int i;
	memcpy(hdr->src_mac, src_mac, sizeof(hdr->src_mac));
	if (type == PKT_RREQ) {
		/* need to construct a frame with broadcast dest addr */
		for (i = 0; i < sizeof(hdr->dst_mac); i++) {
			hdr->dst_mac[i] = 0xFF;
		}
	} else {
		/* get the next hop addr from the table */
		assert(index >= 0);
		memcpy(hdr->dst_mac, rtable[index].next_hop, sizeof(hdr->dst_mac));
	}
	hdr->type_id = htons(ODR_PROTOID);

}

static int get_broadcast_id(char *ip)
{
	int i, br_id = 0;

	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (strcmp(ip, rtable[i].dst_ip) == 0) {
			rtable[i].br_id ++;
			br_id = rtable[i].br_id;
			break;
		}
	}
	return br_id;
}

static void construct_odr_data(struct t_pkt_info *t_info, struct odr_data_pkt *odr_data )
{
	odr_data->type = htonl(t_info->type);
	strcpy(odr_data->src_ip, t_info->src_ip);
	strcpy(odr_data->dst_ip, t_info->dst_ip);
	odr_data->src_port = htons(t_info->src_port);
	odr_data->dst_port = htons(t_info->dst_port);
	odr_data->hop_count = htonl(t_info->hop_count);
	odr_data->msg_sz = htonl(MAX_MSG_SZ);
	odr_data->bcast_id = htonl(t_info->bcast_id);
	odr_data->flag = htonl(t_info->flag);
	odr_data->aux_flag = htonl(t_info->aux_flag);
	memcpy(odr_data->msg_data, t_info->msg, sizeof(odr_data->msg_data));
}

struct ethernet_frame *construct_frame(struct odr_socket_info *info, int ifi_index, struct t_pkt_info *t_info, int index)
{
	int type = t_info->type;
	struct ethernet_frame *frame = Calloc(1, sizeof(*frame));
	
	populate_hdr(&frame->eh, info,  ifi_index, type, index);
	switch(type) {
		case PKT_RREQ:
		case PKT_RREP:
		case PKT_DATA:
			construct_odr_data(t_info, &frame->odr_data);
			break;
		default:
			fprintf(stderr, "this should never happen\n");
			exit(-1);
	}
	return frame;
}

void write_frame(struct ethernet_frame *eframe, struct odr_socket_info *info, int ifi_idx, int pkt_type)
{
	int i;
	struct sockaddr_ll addr;
	struct ethernet_hdr *ehdr = (void *)eframe;
	int sockfd = -1;
	socklen_t len = sizeof(addr);

	memset(&addr, 0x0, sizeof(addr));

	if (pkt_type == PKT_RREQ) 
		addr.sll_pkttype = PACKET_BROADCAST;
	else 
		addr.sll_pkttype = PACKET_OTHERHOST;
	
	addr.sll_family = PF_PACKET;
	addr.sll_ifindex = ifi_idx;
	addr.sll_hatype = ARPHRD_ETHER;
	addr.sll_halen = ETH_ALEN;
	memcpy(addr.sll_addr, ehdr->dst_mac, IF_HADDR);

	for (i = 0; i < info->count; i++) {
		if (info->ifi_idx[i] == ifi_idx) {
			sockfd = info->sockfds[i];
			break;
		}
	}
	assert(sockfd != -1);
	Sendto(sockfd, eframe, sizeof(*eframe), 0 , (void *) &addr, len);
	return;
}

void construct_t_info(struct odr_data_pkt *odr_data, struct t_pkt_info *t_info)
{
	t_info->type = ntohl(odr_data->type);
	t_info->src_port = ntohs(odr_data->src_port);
	t_info->dst_port = ntohs(odr_data->dst_port);
	/* when you receive the packet, update its hop count */
	t_info->hop_count = ntohl(odr_data->hop_count);
	t_info->hop_count += 1;
	strcpy(t_info->src_ip, odr_data->src_ip);
	strcpy(t_info->dst_ip, odr_data->dst_ip);
	t_info->bcast_id = ntohl(odr_data->bcast_id);
	t_info->msg_sz = ntohl(odr_data->msg_sz);
	t_info->flag = ntohl(odr_data->flag);
	t_info->aux_flag = ntohl(odr_data->aux_flag);
	memcpy(t_info->msg, odr_data->msg_data, sizeof(t_info->msg));
}

static inline int is_dst_ip(char *ip1, char *ip2)
{
	if (strcmp(ip1, ip2) == 0)
		return 1;
	else
		return 0;
}

static void xchg_src_dst(struct t_pkt_info *t_info) 
{
	int t_port;
	char t_addr[INET_ADDRSTRLEN];
	t_port = t_info->src_port;
	t_info->src_port = t_info->dst_port;
	t_info->dst_port = t_port;
	strcpy(t_addr, t_info->src_ip);
	strcpy(t_info->src_ip, t_info->dst_ip);
	strcpy(t_info->dst_ip, t_addr);
}

/* the addr contains the mac address from where the pkt came from */
int record_reverse_route(struct t_pkt_info *t_info, struct ethernet_hdr *ehdr, struct sockaddr_ll *addr, int *is_dup)
{
	int i;
	int ret = -1;
	if (strcmp(t_info->src_ip, can_ip) == 0)
		return -1;
	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (strcmp(rtable[i].dst_ip, t_info->src_ip) == 0 ) { 
			/* do further checks */
			/* handle for the case when the route has expired as well */
			if (rtable[i].hop_count > t_info->hop_count || time(NULL) > rtable[i].timestamp + rtable[i].staleness || rtable[i].valid == 0) {
				/* got a more optimal route, store it and update the timestamp, note that this also
				   solves the "new source addr" problem as well since the hop count was initialized
				   for an unfilled entry to be INT_MAX */
				rtable[i].if_idx = addr->sll_ifindex;
				rtable[i].hop_count  = t_info->hop_count;
				memcpy(rtable[i].next_hop, ehdr->src_mac, IF_HADDR);
				rtable[i].timestamp = time(NULL);
				/* this entry is valid from now on */
				rtable[i].valid = 1;
				ret = i;
			} 

			if (rtable[i].br_id < t_info->bcast_id) {
				*is_dup = 0;
				/* regardless of hop count, if we get a bcast_id for the source that is greater
				   than the one in the routing table, then we update the bcast_id in the table */
				rtable[i].br_id = t_info->bcast_id;
				/* if the route discover flag is set, update the next hop regardless of the optimality */
				if (t_info->flag == ROUTE_DISCOVER) {
					memcpy(rtable[i].next_hop, ehdr->src_mac, IF_HADDR);
					rtable[i].if_idx = addr->sll_ifindex;
					rtable[i].timestamp = time(NULL);
					rtable[i].hop_count = t_info->hop_count;
					rtable[i].valid = 1;
					ret = i;
				}
			}
			break;;
		}
	}
	return ret;
}

static int get_index_mac(char *sll_addr)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (memcmp(rtable[i].next_hop, sll_addr, IF_HADDR) == 0) {
			return i;
		}
	}
	return -1;
}

static int record_forward_route(struct t_pkt_info *t_info , struct ethernet_hdr *ehdr, struct sockaddr_ll *addr, int *dup)
{
	int i, ret = -1;
	/* skip this for PKT_DATA */
	if (dup != NULL) {
		for (i = 0; i < ARRAY_SIZE(rtable); i++) {
			if (strcmp(t_info->src_ip, rtable[i].dst_ip) == 0) {
				if (rtable[i].valid)
					*dup = 1;
				else
					*dup = 0;
			}
		}
		if (*dup == 1)
			return i;
	}
	/* if you don't have a route to it then update the table, implies a new RREP */
	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (strcmp(rtable[i].dst_ip, t_info->src_ip) == 0) {
			/* store the route only if it is optimal or if the old entry is stale and update the timestamp */
			if (rtable[i].hop_count > t_info->hop_count || time(NULL) > (rtable[i].timestamp + rtable[i].staleness || rtable[i].valid == 0)) {
				rtable[i].hop_count = t_info->hop_count;
				rtable[i].if_idx = addr->sll_ifindex;
				memcpy(rtable[i].next_hop, ehdr->src_mac, IF_HADDR);
				rtable[i].timestamp = time(NULL);
				rtable[i].valid = 1;
				ret = i;
			}
		}
	}
	return ret;
}

struct t_pkt_info *make_copy(struct t_pkt_info *t)
{
	struct t_pkt_info *t1 = Calloc(1, sizeof(*t1));
	memcpy(t1, t, sizeof(*t1));
	return t1;
}

static void _unlink(struct pkt_queue *t)
{
	struct pkt_queue *fd, *bk;

	fd = t->next;
	bk = t->prev;
	fd->prev = bk;
	bk->next = fd;
}

struct t_pkt_info *pkt_queue_lookup(char *ip)
{
	struct pkt_queue *t = pqueue->next;
	struct t_pkt_info *info;

	while (t != pqueue) {
		info = &t->info;
		if (strcmp(info->dst_ip, ip) == 0) { 
			_unlink(t);
			return info;
		}
		t = t->next;
	}
	return NULL;
}

void pkt_queue_add(struct t_pkt_info *info)
{
	struct pkt_queue *node = Calloc(1, sizeof(*node) );
	memcpy(&node->info, info, sizeof(*info));
	struct pkt_queue *bk, *fd, *head;
	head = pqueue->next;
	fd = head->next;
	bk = head->prev;
	node->prev = bk;
	node->next = fd;
	bk->next = node;
	fd->prev = node;
}

static void print_frame(struct ethernet_frame *e)
{
	printf("############# Ethernet frame header ################\n");
	struct ethernet_hdr *h = (void *)e;
	int i;
	printf("SrcMAC : \n");
	for (i = 0; i < IF_HADDR; i++) {
		printf("%02x:", h->src_mac[i]);
	}
	printf("\nDstMAC :\n");	
	for (i = 0; i < IF_HADDR; i++) {
		printf("%02x:", h->dst_mac[i]);
	}
	printf("###################################################\n");
	
}

static void print_odrpkt(struct odr_data_pkt *data)
{
	printf("############## ODR PACKET #################\n");
	printf("Type : %d\n", ntohl(data->type));
	printf("SrcIP : %s\n", data->src_ip);
	printf("DstIP : %s\n", data->dst_ip);
	printf("Broadcast id : %d\n", ntohl(data->bcast_id));
	printf("Hop count : %d\n", ntohl(data->hop_count));
	printf("###########################################\n");
}

static void print_rtable_entry(struct odr_route_table *entry)
{
	int i;
	printf("====\n");
	printf("Destn IP : %s\n", entry->dst_ip);
	printf("Next hop : \n");
	for (i = 0; i < IF_HADDR; i++) {
		printf("%02x:", entry->next_hop[i]);
	}
	printf("\n");
	printf("Hop count: %d\n", entry->hop_count);
	printf("Interface idx : %d\n", entry->if_idx);
	printf("Broadcast id : %d\n", entry->br_id);
	printf("Staleness is %d\n", entry->staleness);
	printf("====\n");
}

static void print_routing_table()
{
	int i;
	printf("######################## Routing table #######################\n\n");
	for (i = 0; i < ARRAY_SIZE(rtable); i++) {
		if (rtable[i].valid == 1 && strcmp(rtable[i].dst_ip, can_ip) != 0) {
			print_rtable_entry(&rtable[i]);
		}
	}
	printf("##############################################################\n\n");
}


static void print_info(struct ethernet_frame *e)
{
	printf("<ODR> The ODR packet info : \n");
	print_odrpkt(&e->odr_data);
	printf("\n");
	print_routing_table();
	printf("\n");
	printf("<ODR> The ethernet frame header is : \n");
	print_frame((void *)e);
	printf("\n\n");
}

void handle_routing(int sockfd, struct odr_socket_info *info, struct ethernet_frame *ether, struct sockaddr_ll *addr)
{
	struct odr_data_pkt *odr_data = &ether->odr_data;
	struct ethernet_hdr *ehdr = (void *)ether;
	struct t_pkt_info t_info, tt_info;
	int is_dst, index, is_route_recorded = -1;
	struct ethernet_frame *eframe;
	int ignore_idx = addr->sll_ifindex;

	memset(&t_info, 0x0, sizeof(t_info));
	memset(&tt_info, 0x0, sizeof(tt_info));
	/* switch to host byte order so that we can properly compare values */
	construct_t_info(odr_data, &t_info);

	switch(t_info.type) {
		case PKT_RREQ: {
			/* when we receive an RREQ, the ODR can generate two responses, either an RREQ, which
			   it floods across its interface or an RREP back to the interface where it got the RREQ from
			   if it has information about the destination node which is not stale or it is the destination itself */
			/* first record the reverse route */
			int is_dup = 1;
			is_route_recorded = record_reverse_route(&t_info, ehdr, addr, &is_dup);
			printf("<ODR> After recording the reverse route on receiving RREQ, [DUP_RREP : %d]\n", is_dup);
			print_info(ether);

			if (is_dup == 1 ) {
				/* nothing to do, just return */
				return;
			}
	
			/* we check if we are the destn */
			is_dst = is_dst_ip(t_info.dst_ip, can_ip);
			if (is_dst == 1 || ((index = check_route_valid(t_info.dst_ip)) != -1 && t_info.flag != ROUTE_DISCOVER && t_info.aux_flag != RREP_ALREADY_SENT)) {
			/* send an RREP back, make sure the hop count is 0 and src and dst are unicast */

			/* if we made changes to the route due to either getting a route with less hop count or 
			   because the RREQ came from a source which was not recorded in the table, then we flood the 
			   RREQ out, except with a flag saying, dont send RREP back */
				if (is_route_recorded != -1 && !is_dst ) {
					/* need to send the RREQ again, copy the relevant data from t_info */
					memcpy(&tt_info, &t_info, sizeof(tt_info));
					tt_info.aux_flag = RREP_ALREADY_SENT;
					/* flood out the RREQ, in case of RREQ index is always - 1 */
					printf("<ODR> In the intermediate node, Sending the RREQ out, with the 'Dont send RREP' bit set\n");
					int j;
					for (j = 0; j < info->count; j++) {
						if (info->ifi_idx[j] == addr->sll_ifindex)
							continue;
						eframe = construct_frame(info, info->ifi_idx[j], &tt_info, -1);
						/* send the RREQ to all the interfaces except the interface it came on */
						write_frame(eframe, info, info->ifi_idx[j], PKT_RREQ);
						free(eframe);
					}

				}
				xchg_src_dst(&t_info);
				t_info.hop_count = 0;
				t_info.type = PKT_RREP;
				/* So the reason why we are not checking the staleness is, let's say
				   we get here because is_dst == 1, then we can be sure that the reverse route
				   remains valid for this try (why?? because record_reverse_route would
				   update it if it was not valid). If we are at intermediate node, we have already checked
				   whether the route is valid, so there is no need to do the same check again */
				index = check_route_valid_without_staleness(t_info.dst_ip);
				assert(index != -1);

				printf("<ODR> Sending a RREP packet from src: %s to dst: %s\n", get_name(t_info.src_ip), get_name(t_info.dst_ip));
				printf("<ODR> The ethernet frame is :\n");
				eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
				print_frame(eframe);
				/* send the RREP via the interface idx recorded in the routing table */
				write_frame(eframe, info, rtable[index].if_idx, PKT_RREP);
				free(eframe);
			} else {
			 
				/* forward the same RREQ */
				/* this implies that the route was updated, so propagate the update */
				assert(t_info.type == PKT_RREQ);

				printf("<ODR> No route found for dst: %s in %s. Flooding the RREQ"
						" (except on the interface the packet arrived) after increasing the hop count\n", get_name(t_info.dst_ip), get_name(can_ip));
				
				int j;
				for (j = 0; j < info->count; j++) {
					if (info->ifi_idx[j] == addr->sll_ifindex)
						continue;
					eframe = construct_frame(info, info->ifi_idx[j], &t_info, -1);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					/* send the RREQ to all the interfaces except the interface it came on */
					write_frame(eframe, info, info->ifi_idx[j], PKT_RREQ);
					free(eframe);
				}
				
			}
		}
			break;
		
		case PKT_RREP: {
			int is_dup = 0;
			/* first we record the 'forward route' */

			index = record_forward_route(&t_info, ehdr, addr, &is_dup);
			is_dup = lookup_dup_list(&t_info);
			printf("<ODR> After recording the forward route in RREP from src: %s and dst : %s\n", get_name(t_info.src_ip), get_name(t_info.dst_ip));
			print_info(ether);
			
			if (is_dup == 1) {
				printf("<ODR> Duplicate RREP\n");
				return;
			}
			struct t_pkt_info *q_info;
			/* RREP could be received by an intermediate node, in which case we need to forward it. 
			   In the case of the destination node, there could be two cases, it could either be the
			   original destination or this node could have initiated a RREQ when it couldn't find the
			   next hop for an RREP (entry got stale). The way to distinguish is, if it is not the 
			   original destn, then there will be a node with the src-ip in the pkt_queue */
			if (strcmp(can_ip, t_info.dst_ip) == 0 ) {
				if ( (q_info = pkt_queue_lookup(t_info.src_ip)) == NULL) {
					
					printf("<ODR> Sending a DATA pkt in response to RREP packet\n");
					/* this is the original destination, send the data pkt */
					xchg_src_dst(&t_info);
					t_info.hop_count = 0;
					t_info.type = PKT_DATA;
					index = check_route_valid_without_staleness(t_info.dst_ip);
					eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, PKT_DATA);
					free(eframe);
				} else {
					assert(q_info->type != PKT_RREQ);
					printf("<ODR> Got a RREP packet for a pkt in the waitqueue with src : %s and dst: %s\n", get_name(q_info->src_ip), get_name(q_info->dst_ip));
					index = check_route_valid_without_staleness(q_info->dst_ip);
					q_info->bcast_id = t_info.bcast_id + 1;
					eframe = construct_frame(info, rtable[index].if_idx, q_info, index);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, q_info->type);
					free(eframe);
					free(q_info);
				}
			} else {
				/* we are at an intermediate node, we first check if we have  a valid route */
				if ((index = check_route_valid(t_info.dst_ip)) != -1) {
					printf("<ODR> Got a valid route for %s. Forwarding the RREP packet\n", get_name(t_info.dst_ip));
					/* we have a valid route, forward it */
					eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, PKT_DATA); 
					free(eframe);
				} else {
					/* we don't have a valid route, store this in a queue and construct a new RREQ
					   with the intermediate node as src and the original destn as destn */
					printf("<ODR> Unable to find a \"fresh\" route..Adding to the wait queue\n");
					pkt_queue_add(make_copy(&t_info));
					t_info.hop_count = 0;
					t_info.type = PKT_RREQ;
					strcpy(t_info.src_ip, can_ip);
					t_info.bcast_id = get_broadcast_id(t_info.src_ip);
					printf("<ODR> Sending a RREQ with src_ip = %s and dstip = %s\n", get_name(t_info.src_ip), get_name(t_info.dst_ip));
					t_info.flag = t_info.aux_flag = 0;
					int j;
					
					for (j = 0; j < info->count; j++) {
						if (info->ifi_idx[j] == addr->sll_ifindex)
							continue;
						eframe = construct_frame(info, info->ifi_idx[j], &t_info, -1);
						printf("The ethernet frame is :\n");
						print_frame(eframe);
						/* send the RREQ to all the interfaces except the interface it came on */
						write_frame(eframe, info, info->ifi_idx[j], PKT_RREQ);
						free(eframe);
					}
				}
				
			}
			break;
		}
		case PKT_DATA: {
			struct t_pkt_info *q_info;
			char msg[MAX_MSG_SZ];
			int port;
			
			index = record_forward_route(&t_info, ehdr, addr, NULL);
			printf("<ODR> Recording the forward route after receiving DATA pkt\n");
			print_info(ether);
			/* first we check if it is the destination */
			if (strcmp (t_info.dst_ip, can_ip) == 0) {
				/* the src ip will become the dst, so get that index */
				index = check_route_valid_without_staleness(t_info.src_ip);
				assert(index != -1);
				/* if it is the original destn, then we can send the timestamp back */
				if ( (q_info = pkt_queue_lookup(t_info.src_ip)) == NULL) {
					printf("<ODR> Sending data pkt to domain socket\n");
					/* communicate with the server to get the current time */
					msg_send(info->dom_socket, t_info.dst_ip, t_info.dst_port, t_info.msg, t_info.flag );
					if (t_info.src_port == SERVER_PORT ) { 
						return;	
					}
					msg_recv(info->dom_socket, t_info.msg, t_info.dst_ip, &port);
					
					xchg_src_dst(&t_info);
					t_info.hop_count = 0;
					t_info.type = PKT_DATA;
					printf("<ODR> Send the DATA pkt back with the timestamp recorded from the timeserver\n");
					printf("<ODR> The ethernet frame is :\n");
					eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, PKT_DATA);
					free(eframe);
				} else {
					assert(q_info->type != PKT_RREQ);
					printf("<ODR> Got a  packet in response to a DATA pkt in the waitqueu  with src ip = %s and dst_ip = %s\n", get_name(q_info->src_ip), get_name(q_info->dst_ip));
					/* update the broadcast id */
					q_info->bcast_id = t_info.bcast_id + 1;
					eframe = construct_frame(info, rtable[index].if_idx, q_info, index);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, PKT_DATA);
					free(eframe);
					free(q_info);	
				}
			} else {
				/* we are at an intermediate node */
				if ((index = check_route_valid(t_info.dst_ip)) != -1) {
					/* we have a valid route, forward it */
					printf("<ODR> Got a valid route for dst: %s in %s, Forwarding the data packet\n", get_name(t_info.dst_ip), get_name(can_ip));
					eframe = construct_frame(info, rtable[index].if_idx, &t_info, index);
					printf("<ODR> The ethernet frame is :\n");
					print_frame(eframe);
					write_frame(eframe, info, rtable[index].if_idx, t_info.type);
					free(eframe);
				} else {
					/* we don't have a valid route, store this in a queue and construct a new RREQ
					   with the intermediate node as src and the original destn as destn */
					pkt_queue_add(make_copy(&t_info));
					printf("<ODR> Unable to find a \"fresh\" route, for  srcIP = %s and dstIP = %s\n", get_name(t_info.src_ip), get_name(t_info.dst_ip));
					printf("<ODR> Storing the pkt in the wait queue\n");
					t_info.hop_count = 0;
					t_info.type = PKT_RREQ;
					strcpy(t_info.src_ip, can_ip);
					t_info.bcast_id = get_broadcast_id(t_info.src_ip);
					printf("<ODR> Sending a RREQ with src_ip = %s and dstip = %s\n", get_name(t_info.src_ip), get_name(t_info.dst_ip));
					t_info.flag = t_info.aux_flag = 0;
					int j;
					for (j = 0; j < info->count; j++) {
						if (info->ifi_idx[j] == addr->sll_ifindex)
							continue;
						eframe = construct_frame(info, info->ifi_idx[j], &t_info, -1);
						/* send the RREQ to all the interfaces except the interface it came on */
						printf("<ODR> The ethernet frame is :\n");
						print_frame(eframe);
						write_frame(eframe, info, info->ifi_idx[j], PKT_RREQ);
						free(eframe);
					}
				}
			}
			break;
		}
		default:
			printf("Warning: invalid type\n");
			break;
	}

	return;
}
#endif
