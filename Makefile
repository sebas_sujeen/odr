CC = gcc
CFLAGS = -g -I/users/cse533/Stevens/unpv13e/lib/ -Wall -Wno-unused-variable -Wno-unused-function
LIBS = /users/cse533/Stevens/unpv13e/libunp.a -lpthread

all: client server odr

server: server.o
	${CC} ${CFLAGS} -o server_3254 server.o 	${LIBS}
server.o: server.c
	${CC} ${CFLAGS} -c server.c

client: client.o
	${CC} ${CFLAGS} -o client_3254 client.o ${LIBS}
client.o: client.c
	${CC} ${CFLAGS} -c client.c

odr: odr.o
	${CC} ${CFLAGS} -o odr_3254 odr.o ${LIBS}
odr.o: odr.c
	${CC} ${CFLAGS} -c odr.c

clean:
	rm -f *.o server_3254 client_3254 odr_3254
