#include "common_defines.h"


int main(int argc, char **argv)
{
	int sockfd, flag = 0, count = 0; 
	/* define as an array instead of a pointer to make the string writable */
	char template[] = "/tmp/tmpfile_XXXXXX";
	char vm_name[HOST_NAME_MAX];
	char lhost_name[HOST_NAME_MAX];
	char *rhost_name, *canonical_ip;
	char canonical_ip_src[INET_ADDRSTRLEN];
	char res_buf[MAX_MSG_SZ];
	int src_port;
	char msg[] = "xx";
	uint16_t dest_port;

	srand(time(NULL));
	/* initialize the client socket */
	mkstemp(template);
	unlink(template);
	sockfd = bind_domain_socket(template);

	/* handle user interaction */
	while (1) {
		printf("<Client> Enter one of the 10 vm's address to communicate with\n");
		Fgets(vm_name, sizeof(vm_name), stdin);

		get_lhostname(lhost_name, sizeof(lhost_name));
		printf("<Client> Node %s sending request to server at %s\n", lhost_name, vm_name);
		/* get the canonical ip of the dst */
		canonical_ip = get_canonical_ip(vm_name);
		/* get an ephemereal port for the client, this is used as a key for the 
		 * client's sun_path, the ODR process maintains this relationship in 
		 * a table */
again:
		dest_port = get_eph_port();
		if (dest_port == SERVER_PORT)
			goto again;

		while (count <= 1) {
			/* call msg_send to communicate with the ODR protocol */
			msg_send(sockfd, canonical_ip, dest_port, msg, flag);

			/* here we wait in msg_recv() backed by timeout */
			fd_set rset;
			int max_fd, ret;
			struct timeval tv;

			tv.tv_sec = 5;
			tv.tv_usec = 0;
			max_fd = sockfd + 1;

			FD_ZERO(&rset);
			FD_SET(sockfd, &rset);
			ret = _select(max_fd, &rset, NULL, NULL, &tv);
			if (ret == 0) {
				/* we got here due to a timeout, send the msg again but now with the flag to force route discovery */
				count ++ ;
				flag = ROUTE_DISCOVER;
				printf("<Client> node %s: timeout on response from %s, Setting the FORCE_DISCOVER flag\n", lhost_name, vm_name);
				continue;
			} else if (FD_ISSET(sockfd, &rset)) {
				msg_recv(sockfd, res_buf, canonical_ip_src, &src_port);
				printf("<Client> node %s: received from %s\n", lhost_name, vm_name);
				printf("<Client> Received msg is %s\n", res_buf);
				/* break out of while loop */
				break;
			}
		}
		break;
	}
	unlink(template);
	printf("<Client> Exiting now\n");
	return 0;
}
